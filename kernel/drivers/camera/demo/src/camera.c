#define __DEFINED_suseconds_t
#define __DEFINED_time_t
typedef long time_t;
typedef long suseconds_t;

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/videodev2.h>
#include <string.h>
#include <sys/mman.h>
#include <errno.h>
#include <pthread.h>
#include <linux/fb.h>
#include <stdbool.h>

int camera_width = 1200;  //camere屏幕宽
int camera_height = 1920;   //camera屏幕高
 
int main(void)
{	
	//1.打开摄像头设备
	int fd = open("/dev/video0",O_RDWR);  //video0 或 video1
	if(fd < 0)
	{
		perror("11111111111111");
		perror("打开设备失败");
		return -1;
	}

	struct v4l2_format vfmt; 
	vfmt.type=V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE; //设置类型摄像头采集
	vfmt.fmt.pix.width = camera_width;  //设置宽
	vfmt.fmt.pix.height = camera_height;   //设置高
	vfmt.fmt.pix_mp.pixelformat = V4L2_PIX_FMT_YUYV;
	int ret = ioctl(fd, VIDIOC_S_FMT, &vfmt);
	if(ret < 0)
	{
		perror("222222222222222");
		perror("设置格式失败");
	}	
	printf("Current data format information:\n\twidth:%d\n\theight:%d\n",vfmt.fmt.pix.width,vfmt.fmt.pix.height);
	printf("\n\tnum_planes:%d\n\t\n",vfmt.fmt.pix_mp.num_planes);

	struct v4l2_requestbuffers reqbuffer; 
	reqbuffer.count = 4;   //申请4个缓冲区  
	reqbuffer.type=V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;  // 缓冲帧数据格式
	reqbuffer.memory=V4L2_MEMORY_MMAP;   //内存映射
	ret = ioctl(fd, VIDIOC_REQBUFS, &reqbuffer);
	if(ret < 0)
	{
		perror("33333333333333333");
		perror("申请队列空间失败");
	}
	
	struct plane_start {
		void * start;
	};

	struct buffer {
		struct plane_start* plane_start;
		struct v4l2_plane* planes_buffer;
	};

	int num_planes = vfmt.fmt.pix_mp.num_planes;

	struct buffer *buffers = malloc(reqbuffer.count * sizeof(struct buffer));
	struct v4l2_buffer mapbuffer;

	struct v4l2_plane *planes_buffer;
	struct plane_start *plane_start;

	for (int i = 0; i < reqbuffer.count; i++)  //count=4个缓冲区
	{
		memset(&mapbuffer, 0, sizeof(mapbuffer));
		planes_buffer = (struct v4l2_plane*)calloc(num_planes, sizeof(struct v4l2_plane));
		plane_start = (struct plane_start*)calloc(num_planes, sizeof(struct plane_start));
		memset(planes_buffer, 0, sizeof(*planes_buffer));

		mapbuffer.type = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
		mapbuffer.memory = V4L2_MEMORY_MMAP;
		mapbuffer.m.planes = planes_buffer;
		mapbuffer.length = num_planes;
		mapbuffer.index = i;
		// 查询序号为i 的缓冲区，得到其起始物理地址和大小
		if (ioctl (fd, VIDIOC_QUERYBUF, &mapbuffer) == -1) {
			perror("44444444444444444444444444444");
			printf("Querybuf fail\n");
		}
		(buffers + i)->planes_buffer = planes_buffer;
		(buffers + i)->plane_start = plane_start;
		for(int j = 0; j < num_planes; j++) {
			printf("plane[%d]: length = %d\n", j, (planes_buffer + j)->length);
			printf("plane[%d]: offset = %d\n", j, (planes_buffer + j)->m.mem_offset);
			(plane_start + j)->start = mmap (NULL /* start anywhere */,
								(planes_buffer + j)->length,
								PROT_READ | PROT_WRITE /* required */,
								MAP_SHARED /* recommended */,
								fd,
								(planes_buffer + j)->m.mem_offset);
			if ((plane_start +j)->start == MAP_FAILED) {
				printf ("mmap failed\n");
			}
		}

	}
	

	struct v4l2_buffer buf;
	for (unsigned int i = 0; i < reqbuffer.count; ++i) {
		memset(&buf, 0, sizeof(buf));

		buf.type        = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
		buf.memory      = V4L2_MEMORY_MMAP;
		buf.length   	= num_planes;
		buf.index       = i;
		buf.m.planes 	= (buffers + i)->planes_buffer;

		if (ioctl (fd, VIDIOC_QBUF, &buf) < 0) {
			perror("55555555555555555555555555");
			printf ("VIDIOC_QBUF failed\n");
		}
	}

	
	int type = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
	ret = ioctl (fd, VIDIOC_STREAMON, &type); 
	if(ret < 0)
	{
		perror("666666666666666666666666");
		perror("开启失败");
	}
	//定义一个空间存储解码后的RGB数据
	//unsigned char rgbdata[camera_width*camera_height*3];

	struct v4l2_plane *tmp_plane;
	tmp_plane = calloc(num_planes, sizeof(*tmp_plane));
	printf("VIDIOC_DQBUF start");
	while (1)
	{
		memset(&buf, 0, sizeof(buf));
		buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
		buf.memory = V4L2_MEMORY_MMAP;
		buf.m.planes = tmp_plane;
		buf.length = num_planes;
		if (ioctl (fd, VIDIOC_DQBUF, &buf) < 0)
			printf("dqbuf fail\n");

		FILE *file = fopen("my.yuyv","w+");
		perror("my.yuyv file error num:");
		printf("num_planes:%d\n",num_planes);
		for (int j = 0; j < num_planes; j++) {
			printf("plane[%d] start = %p, bytesused = %d\n", j, ((buffers + buf.index)->plane_start + j)->start, (tmp_plane + j)->bytesused);
			fwrite(((buffers + buf.index)->plane_start + j)->start, (tmp_plane + j)->bytesused, 1, file);
		}

		if (ioctl (fd, VIDIOC_QBUF, &buf) < 0)
			printf("failture VIDIOC_QBUF\n");
	}




/**
	while(1)
	{
		//7.采集数据
		//从队列中提取一帧数据
		struct v4l2_buffer readbuffer;
		readbuffer.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		readbuffer.memory = V4L2_MEMORY_MMAP;
		ret = ioctl (fd, VIDIOC_DQBUF, &readbuffer); // 从缓冲区取出一个缓冲帧
		if(ret < 0)
		{
			perror("77777777777777777777");
			perror("提取数据失败");
		}
		
		  //把一帧数据写入文件
		FILE *file = fopen("my.yuyv","w+");
		//mptr[readbuffer.index]
		fwrite(mptr[readbuffer.index],readbuffer.length,1,file);
		fclose(file);
		
		//通知内核已经使用完毕
		ret = ioctl (fd,VIDIOC_QBUF,&readbuffer);
		if(ret < 0)
		{
			perror("888888888888888888888");
			perror("放回队列失败");
		}
	}
**/
	//8.停止采集
	type = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;

	if (ioctl(fd, VIDIOC_STREAMOFF, &type) < 0) {
		perror("999999999999999999999");
		printf("VIDIOC_STREAMOFF fail\n");
	}
 

	for (int i = 0; i < reqbuffer.count; i++) {
		for (int j = 0; j < num_planes; j++) {
			if (MAP_FAILED != ((buffers + i)->plane_start + j)->start) {
				if (-1 == munmap(((buffers + i)->plane_start + j)->start, ((buffers + i)->planes_buffer + j)->length))
					printf ("munmap error\n");
			}
		}
	}

	//9.释放映射
	for (int i = 0; i < reqbuffer.count; i++) {
	    free((buffers + i)->planes_buffer);
		free((buffers + i)->plane_start);
	}
	
	//10.关闭设备
	close(fd);
	
	return 0;	
}
 
