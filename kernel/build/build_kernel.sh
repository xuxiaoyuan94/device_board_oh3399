#!/bin/bash

# Copyright (c) 2021 opintelink Open Source Organization .
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

set -e

pushd ${1}
ROOT_DIR=${5}
export PRODUCT_PATH=${4}
export DEVICE_COMPANY=${6}
export DEVICE_NAME=${7}
export PRODUCT_COMPANY=${8}

KERNEL_SOURCE=${ROOT_DIR}/device/board/opintelink/kernel/kernel-5.10
UBOOT_CONFIG_SOURCE=${ROOT_DIR}/device/board/opintelink/oh_3399/config
KERNEL_OBJ_TMP_PATH=${ROOT_DIR}/out/kernel/OBJ/linux-5.10
KERNEL_SRC_TMP_PATH=${ROOT_DIR}/out/kernel/src_tmp/linux-5.10
KERNEL_HDF_PATCH_PATH=${ROOT_DIR}/device/board/opintelink/kernel/hdf_patch/linux-5.10
LINUX_CONFIG_FILE=${ROOT_DIR}/device/board/opintelink/kernel/kernel_config/linux-5.10/nanopi4_linux_defconfig

rm -rf ${KERNEL_SRC_TMP_PATH}
mkdir -p ${KERNEL_SRC_TMP_PATH}

rm -rf ${KERNEL_OBJ_TMP_PATH}
mkdir -p ${KERNEL_OBJ_TMP_PATH}
export KBUILD_OUTPUT=${KERNEL_OBJ_TMP_PATH}

cp -arf ${KERNEL_SOURCE}/* ${KERNEL_SRC_TMP_PATH}/

cd ${KERNEL_SRC_TMP_PATH}

#打hdf补丁文件
patch -p1 < ${KERNEL_HDF_PATCH_PATH}/hdf.patch

bash ${KERNEL_HDF_PATCH_PATH}/patch_hdf.sh ${ROOT_DIR} ${KERNEL_SRC_TMP_PATH} ${DEVICE_NAME}

cp -rf ${LINUX_CONFIG_FILE} ${KERNEL_SRC_TMP_PATH}/arch/arm64/configs/nanopi4_linux_defconfig

./make-ohos.sh nanopi4_linux_defconfig

mkdir -p ${2}

cp ${KERNEL_OBJ_TMP_PATH}/resource.img ${2}/resource.img
cp ${KERNEL_SRC_TMP_PATH}/boot_linux.img ${2}/boot_linux.img

cp ${UBOOT_CONFIG_SOURCE}/config.cfg ${2}/config.cfg
cp ${UBOOT_CONFIG_SOURCE}/MiniLoaderAll.bin ${2}/MiniLoaderAll.bin
cp ${UBOOT_CONFIG_SOURCE}/parameter.txt ${2}/parameter.txt
cp ${UBOOT_CONFIG_SOURCE}/trust.img ${2}/trust.img
cp ${UBOOT_CONFIG_SOURCE}/uboot.img ${2}/uboot.img
cp ${UBOOT_CONFIG_SOURCE}/config_ramdisk.cfg ${2}/config_ramdisk.cfg
cp ${UBOOT_CONFIG_SOURCE}/parameter_ramdisk.txt ${2}/parameter_ramdisk.txt

popd